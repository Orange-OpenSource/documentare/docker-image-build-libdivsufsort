#!/bin/sh

JDK_PACKAGE=openjdk-11-jdk-headless
JDK_INCLUDE_DIRECTORY_PATH=./usr/lib/jvm/java-11-openjdk-amd64/include/
TARGET_INCLUDE_PATH=/usr/include/jni/

echo
echo "=== Will use ${JDK_PACKAGE}"
echo "=== Will install JNI include files here: ${TARGET_INCLUDE_PATH}"
echo

apt-get download "${JDK_PACKAGE}" && \
DOWNLOADED_PACKAGE_PATH=$(pwd)$(ls openjdk-11-jdk-headless*) && \
echo Downloaded package found here: "${DOWNLOADED_PACKAGE_PATH}" && \
\
echo && \
echo Extract package and install include files to "${TARGET_INCLUDE_PATH}" && \
mkdir -p "${TARGET_INCLUDE_PATH}" && \
cd /tmp && mkdir extract && cd extract && ar x "${DOWNLOADED_PACKAGE_PATH}" && tar xvf data.tar.xz "${JDK_INCLUDE_DIRECTORY_PATH}" && cp -r "${JDK_INCLUDE_DIRECTORY_PATH}" "${TARGET_INCLUDE_PATH}" && \
\
rm -rf "${DOWNLOADED_PACKAGE_PATH}" /tmp/extract && \
echo === OK
