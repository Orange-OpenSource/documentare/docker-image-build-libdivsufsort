FROM debian:bullseye-slim

RUN set -ex; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
		binutils xz-utils \
		gcc-10 \
		g++-10 \
        cmake \
        make;

ADD ./install-jni-headers.sh ./
RUN ./install-jni-headers.sh && apt-get clean && rm -r /var/lib/apt/lists/*
